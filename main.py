# Para poder importar las librerías que están en esta hoja de código, instalar las siguientes
# pip install pandas
# pip install gensim
# pip install wordcloud
# pip install pyLDAvis

import pandas as pd
pd.options.display.max_columns =None
pd.options.display.max_rows = None


import gensim.corpora as corpora

from wordcloud import WordCloud

from pprint import pprint

from gensim.models.ldamodel import LdaModel

import numpy as np

from gensim.models.coherencemodel import CoherenceModel

import pyLDAvis
import pyLDAvis.gensim



# Muestra la versión de Pandas instalada
def main():
  print(pd.__version__)

  # Se carga el archivo de DENUE
  df = pd.read_csv("denue_2022_5yrs_nombre_sim.csv") 

  # Se Extraen unicamente las columnas de Clave de la Colonia, Clave de la Actividad Económica 
  # y el Nombre de la Actividad Económica
  df2 = df[['colonia_cv','actividad_','nombre_sim']].copy()

  # Muestra el DataFrame con las columnas seleccionadas
  print(df2.head())


  # En los campos que no se tengan registros, sustituirlo por 0's
  df2 = df2.fillna(0)

  # Conversión de colonia_cv de float a int y conversión de las otras columnas a String
  df2['colonia_cv'] = df2['colonia_cv'].astype(int)
  df2['actividad_'] = df2['actividad_'].astype(str)


  #################################################
  #Tratamiento del df2 para crear la Nube de Palabras con la Clave de Actividad

  df_wordscl = df2[["colonia_cv","nombre_sim"]]
  df_wordscl.replace("\(mayoreo\)", "al mayoreo", regex=True,inplace=True)
  df_wordscl.replace(" ", "_", regex=True,inplace=True)
  
  
  # La Nube de Palabras funciona con letras, pero como se esta trabajando con números, para que se pueda generar la Nube de Palabras se le va a asignar un prefijo con una letra a cada clave, para que detecte al menos una letra y la clave sea procesada como palabra
  #df_wordscl["actividad_"] = "a_" + df_wordscl["actividad_"]
  
  groups = df_wordscl.groupby('colonia_cv')['nombre_sim'].apply(list)
  df_wordscl3 = groups.reset_index(name  = 'list_actividad')
  df_wordscl3['nueva_lista_actividad'] = df_wordscl3['list_actividad'].apply(lambda x: list(set(x)))
  df_wordscl3 = df_wordscl3[['colonia_cv', 'nueva_lista_actividad']].copy()
  df_wordscl3['array_en_string_actividad'] =[' '.join(map(str, l)) for l in df_wordscl3['nueva_lista_actividad']]
  df_wordscl4 = df_wordscl3[['array_en_string_actividad']].copy()
  #Fin de tratamiento del df2 para crear la Nube de Palabras


  #Preparando el DataFrame df_wordscl4 para crear la Nube de Palabras

  ### Las agrupaciones de los codigos por colonia se meten a un arreglo
  long_string = ','.join(list(df_wordscl4['array_en_string_actividad'].values))
  long_string2 = long_string.replace(' ', ',')
  long_string3 = long_string2.replace('_', ' ')

  # Creación de un objeto WordCloud
  wordcloud = WordCloud(background_color="white", max_words=1000, contour_width=3, contour_color='steelblue', width=800, height=400, collocations=False, regexp=r"[^,']+")

  # Se genera la Nube de Palabras a partir del arreglo de agrupaciones
  wordcloud.generate(long_string3)

  # Se visualiza la Nube de Palabras
  img = wordcloud.to_image()
  img.show()

  ##################################################
  # Fin de la Nube de Palabras



  # en el campo de activida_1 se reemplazan los espacios por guion bajo, 
  # esto para concatenarlo con el campo de actividad_ y se quede como si esto fuera "una sola palabra"
  df2.replace(" ", "_", regex=True,inplace=True)
  df2["actividad_cve_nom"] = df2["actividad_"].astype(str) + "_" + df2["nombre_sim"].astype(str)	

  # Se agrupan las Actividades Económicas por Colonia
  groups = df2.groupby('colonia_cv')['actividad_cve_nom'].apply(list)
  df3 = groups.reset_index(name  = 'listvalues')

  # Se crea una columna sin valores repetidos de Actividades de cada arreglo
  df3['nueva_lista'] = df3['listvalues'].apply(lambda x: list(set(x)))

  # Se modifica el DataFrame dejando la columna de las claves de colonias y la columna con los nuevos arreglos
  # sin valores repetidos
  df3 = df3[['colonia_cv', 'nueva_lista']].copy()

  #Se crea un DataFrame con una sola columna, que es la que contiene las agrupaciones de actividades económicas 
  # ya sin valores repetidos
  df4 = df3[['nueva_lista']].copy()
  print("df4")
  print(df4.head())

  # Se crea un array de las agrupaciones
  array_unico = df4.nueva_lista.tolist()

  array_unico_s = [t for t in array_unico]

  
  # reemplazando los guiones bajos por espacios
  for i in range (len(array_unico_s)):
      array_unico_s[i] = [s.replace('_', ' ') for s in array_unico_s[i]]

  # Se crea un diccionario en el cual en cada agrupación se tokeniza cada Actividad Económica asignandole una clave y se le asocia a la frecuencia con la que esta Actividad aparece por arreglo
  id2word = corpora.Dictionary(array_unico_s)
  corpus = [id2word.doc2bow(text) for text in array_unico_s]


  # Algoritmo del LDA con 6 categorías
  lda_model = LdaModel(corpus=corpus,
                    id2word=id2word,
                    num_topics=6, 
                    random_state=100,
                    update_every=1,
                    chunksize=100,
                    alpha='auto',
                    per_word_topics=True)


  pprint(lda_model.print_topics())

  # Imprime cada categoría con las actividades económicas predominantes
  print(lda_model[corpus])

  print("__________________________")

  # Se crea un arreglo de colonias con las probabilidades de pertenecer a una categoría 
  arreglo_probabilidades = []
  for i in range (len(array_unico_s)):
    arreglo_probabilidades.append(lda_model[corpus[i]][0])
    arreglo_probabilidades[i].sort(key=lambda a:a[1],reverse=True)

    # Quitar la línea de código para imprimir el arreglo de probabilidades de colonias
    #print(arreglo_probabilidades[i][0][0]+1," ",arreglo_probabilidades[i])
    
  print("__________________________")

  # Este bloque obtiene el valor mas alto del arreglo de probabilidades y obtiene la categoría por colonia, 
  # así mismo cada uno de estos valores se mete en un arreglo de categorías
  lista_frecuencia = []
  for i in range(len(array_unico_s)):

    rango = int(len(arreglo_probabilidades[i]))
    arreglo_k = []
    arreglo_v = []
    for j in range (rango):
      arreglo_k.append(arreglo_probabilidades[i][j][0])
      arreglo_v.append(arreglo_probabilidades[i][j][1])

    for k in range (rango):
      if(np.max(arreglo_v)==(arreglo_v[k])):
        lista_frecuencia.append(arreglo_k[k]+1)
        #print(arreglo_k[k]+1)
        
  print("____________________________")

  # Del arreglo de probabilidades, este bloque genera un arreglo alternativo de categorías que 
  # obtiene el segundo valor mas alto en caso de existir por colonia
  lista_frecuencia_alternativa = []
  for i in range(len(array_unico_s)):

    arreglo_probabilidades[i].sort(key=lambda a:a[1],reverse=True)
    #print(arreglo_probabilidades[i])

    if( len(arreglo_probabilidades[i]) == 1):
      lista_frecuencia_alternativa.append(arreglo_probabilidades[i][0][0]+1)
      #print(arreglo_probabilidades[i][0][0]+1," ",arreglo_probabilidades[i])
    else:
      lista_frecuencia_alternativa.append(arreglo_probabilidades[i][1][0]+1)
      #print(arreglo_probabilidades[i][1][0]+1," ",arreglo_probabilidades[i])
      
  print("____________________________")

  ### Este bloque concatena los arreglos de categorías al DataFrame de Colonias, 
  # esto para mostrar a que categoría pertenece cada colonia
  df_frecuencia = pd.DataFrame(lista_frecuencia, columns=['tema'])
  df_frecuencia_alternativa = pd.DataFrame(lista_frecuencia_alternativa, columns=['tema_alternativo'])
  df_colonias = df3[['colonia_cv']]
  df_colonias_frecuencia = pd.concat([df_colonias,df_frecuencia,df_frecuencia_alternativa], axis=1)
  df_colonias_frecuencia.head(30)

  # Se crea un archivo csv de las colonias con las categorías a las que pertenecen
  df_colonias_frecuencia.to_csv('colonias_con_categoria.csv', index=False) 


  # Este bloque crea un DataFrame que muestra la frecuencia con la que aparece cada categoría
  df_frecuencia = pd.DataFrame(lista_frecuencia, columns=['tema'])
  df_frecuencia_porcentaje = pd.DataFrame(df_frecuencia.groupby(['tema'])['tema'].count())
  df_frecuencia_porcentaje.rename(columns={'tema':'frecuencia'}, inplace = True)
  df_frecuencia_porcentaje['porcentaje'] = (df_frecuencia_porcentaje['frecuencia']*100)/(len(array_unico_s))

  print(df_frecuencia_porcentaje.sort_values(by='porcentaje', ascending=False))

  print("______________________________________________________")

  # Este bloque crea un DataFrame que muestra la frecuencia con la que aparece cada categoría alternativa
  df_frecuencia_temas_alternativos = pd.DataFrame(lista_frecuencia_alternativa, columns=['tema_alternativo'])

  df_frecuencia_alternativa_porcentaje = pd.DataFrame(df_frecuencia_temas_alternativos.groupby(['tema_alternativo'])['tema_alternativo'].count())
  df_frecuencia_alternativa_porcentaje.rename(columns={'tema_alternativo':'frecuencia'}, inplace = True)
  df_frecuencia_alternativa_porcentaje['porcentaje'] = (df_frecuencia_alternativa_porcentaje['frecuencia']*100)/(len(array_unico_s))

  print(df_frecuencia_alternativa_porcentaje.sort_values(by='porcentaje', ascending=False))

  print("____________________________")
  
  # Muesta el coeficiente de coherencia
  coherence_model_lda = CoherenceModel(model=lda_model, texts=array_unico_s, dictionary=id2word, coherence='c_v')
  coherence_lda = coherence_model_lda.get_coherence()
  print('\nCoherence Score: ', coherence_lda)     
  
  # Crea y guarda la gráfica del LDA en la carpeta raíz
  vis_data = pyLDAvis.gensim.prepare(lda_model, corpus, id2word, sort_topics=False) #, sort_topics=False
  pyLDAvis.save_html(vis_data, './lda.html')
  
  
# Ejecuta todo el programa
if __name__ == "__main__":
  main()